﻿namespace Paises.Servicos
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Junta uma coleção de métodos que lida diretamente com 'operações relacionadas com o mapa'.
    /// </summary>
    public class MapService
    {
        double posY;

        /// <summary>
        /// Converte as coordenadas latitude/longitude em pixeis/unidade utilizada pelo WF.
        /// </summary>
        /// <remarks>
        /// A longitude é de conversão direta, mas a latitude (Height) requereu uma formula logaritmica
        /// porque as distancias tendem para o infinito nos polos neste tipo de projeção
        /// </remarks>
        /// <param name="lng"></param>
        /// <param name="lat"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pictureBoxPin"></param>
        /// <returns>Devolve o ponto com as coordenadas respectivas, em pixeis.</returns>
        public Point CoordsToMapa(double lng, double lat, int width, int height, PictureBox pictureBoxPin)
        {
            //Y
            int mapWidth = width;
            int mapHeight = height;
            double latRad = (lat * Math.PI) / 180;
            double mercN = Math.Log(Math.Tan((Math.PI / 4) + (latRad / 2)));
            double y = ((mapHeight / 2) - (mapWidth * mercN / (2 * Math.PI)));

            //X
            double lngDegree = mapWidth / 360.0;
            double x = (lng + 180) * lngDegree - pictureBoxPin.Width / 2;

            Point ponto = new Point((int)x, (int)y);

            return ponto;
        }

        /// <summary>
        /// Método inverso ao anterior.
        /// </summary>
        /// <remarks>
        /// Calculada a partir da outra fórmula, parece ter equivalência, 
        /// mas duvido porque é a primeira forma... Não implementado, apenas testes.
        /// </remarks>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pictureBoxPin"></param>
        /// <returns></returns>
        public List<Double> MapaToCoords(int x, int y, int width, int height, PictureBox pictureBoxPin)
        {
            int mapWidth = width;
            int mapHeight = height;
            double mercN = ((mapHeight / 2) - y) * (2 * Math.PI) / mapWidth;
            double latRad = 2 * (Math.Atan(Math.Pow(Math.E, mercN)) - Math.PI / 4);
            double lat = latRad * 180 / Math.PI;

            double lngDegree = mapWidth / 360.0;
            double lng = x / lngDegree + pictureBoxPin.Width / 2 - 180;

            var coords = new List<Double> { lng, lat };

            return coords;
        }


        /// <summary>
        /// Posiciona os fusos horários no mapa, na métrica dos marcadores.
        /// </summary>
        /// <param name="zoneTime"></param>
        /// <param name="width"></param>
        /// <returns>Devolve a posX (.Left) do painel de Fuso Horário.</returns>
        public int timeZoneLocX(string zoneTime, int width)
        {

            zoneTime = zoneTime.Remove(0, 3);
            string[] zT = zoneTime.Split(':');
            zoneTime = string.Join(".", zT);
            double tZ = Convert.ToDouble(zoneTime, System.Globalization.CultureInfo.InvariantCulture);

            double unit = width / 25;

            posY = tZ * unit + 11.5 * unit - 5;

            var exced = 11.5 * unit - 5 - tZ * unit;

            if ((posY - exced) % unit != 0)
                return (int)(posY - posY % unit + unit / 2) + 10;
            else
                return (int)posY;
        }


        /// <summary>
        /// Posiciona os marcadores da timezone/fuso horário.
        /// </summary>
        /// <remarks>
        /// O ratio para este mapa é sensivelmente a largura a dividir por 25 - do -12 ao 12. 
        /// São adicionadas + 2 TZ à direita, que existem.
        /// </remarks>
        /// <param name="tZ">Valor da time zone (UTC)</param>
        /// <param name="width">Largura do mapa</param>
        /// <returns></returns>
        public int timeZoneLocX2(decimal tZ, int width)
        {

            int unit = width / 25;

            posY = (int)tZ * unit + 12 * unit;

            //MessageBox.Show(unit.ToString());

            return (int)posY;
        }


    }
}
