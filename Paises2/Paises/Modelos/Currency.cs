﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paises.Modelos
{
    public class Currency
    {
        public string code { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
    }
}
