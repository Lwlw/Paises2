﻿namespace Paises
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using static System.Windows.Forms.ListView;

    public partial class Form1 : Form
    {

        bool load;
        bool tzCreated;
        Panel[] panelsTZ;
        Label[] labelsTZ;
        PictureBox[] pinBorders;
        Label[] labelBorders;
        const int originalSize = 700;
        int sortColumn;
        int nListVItems;
        

        Country paisB = new Country();
        private List<Country> Paises;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;
        private ImageService imageService;
        private MapService mapService;

        public object AutocompleteSource { get; private set; }


        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            imageService = new ImageService();
            mapService = new MapService();
            LoadPaises();

        }


        private void Form1_Load(object sender, EventArgs e)
        {

            #region Resizing Possibility

            Image img = Image.FromFile(@"..\..\Resources\mapaMundiSqrsMercator.png");
            Image img2 = Image.FromFile(@"..\..\Resources\mapaMundiSqrsMercatorTrq.png");

            panelMapa.BackgroundImage = imageService.ResizeImage(img, 700, 700);
            panelMapa.Size = new Size(panelMapa.BackgroundImage.Width, panelMapa.BackgroundImage.Height);

            BackgroundImage = imageService.ResizeImage(img2, 700, 700);
            ClientSize = new Size(Width, BackgroundImage.Height);
            
            #endregion


            checkBoxFusos.Location = new Point(panelMapa.Right - checkBoxFusos.Width, panelMapa.Top + 2);
            checkBoxFronteiras.Location = new Point(checkBoxFusos.Left - checkBoxFronteiras.Width - 5, panelMapa.Top + 2);
            checkBoxSearch.Location = new Point(checkBoxFronteiras.Left - checkBoxSearch.Width - 5, panelMapa.Top + 2);

            panelInfo.BackColor = Color.FromArgb(150, Color.DarkTurquoise);
            pictureBoxPin.BackColor = Color.FromArgb(200, Color.DarkTurquoise);
            panelTZ.BackColor = Color.FromArgb(50, Color.White);
            panelTZ.Width = panelMapa.Width / 25;
            listView1.BackColor = Color.Black;
            listView1.ForeColor = Color.DarkGray;

            panelSearch.Parent = this;
            panelSearch.Left = Width / 2 - panelSearch.Width / 2;
            panelSearch.BackColor = Color.FromArgb(150, Color.DarkTurquoise);

            panelMapa.AllowDrop = true;
            foreach (Control c in panelMapa.Controls)
            {
                c.MouseDown += new MouseEventHandler(c_MouseDown);
            }


            panelMapa.DragOver += new DragEventHandler(panelMapa_DragOver);
            panelMapa.DragDrop += new DragEventHandler(panelMapa_DragDrop);

            progressBar1.Left = Width / 2 - progressBar1.Width / 2;
            labelACarregar.Left = progressBar1.Left;
        }


        /// <summary>
        /// Método 'assíncrono' através do qual é carregada a lista de Paises, 
        /// através da ligação à API ou localmente a partir da DB.
        /// </summary>
        /// <remarks>
        /// É testada a ligação à internet.
        /// Quando não existe conexão, carrega as listas desde a base de dados.
        /// Quando existe, carrega as listas e actualiza a base de dados Sqlite.
        /// Se não existe base de dados nem internet, dá o aviso e termina a execução.
        /// </remarks>
        private async void LoadPaises()
        {

            labelACarregar.Text = "A atualizar os países...";

            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                ControlsVisibility(false);
                LoadLocalPaises();
                load = false;
            }
            else
            {
                ControlsVisibility(false);
                await LoadApiPaises();
                load = true;
                
            }



            if (Paises.Count == 0)
            {
                labelACarregar.Text = "Não há ligação à Internet\n" +
                    "e não foram previamente carreagados os países" + Environment.NewLine +
                    "Tente mais tarde!";

                labelStatus.Text = "Primeira inicialização requer acesso à Internet";
                return;
            }

            
            PopulateCombo();
            CarregaCombo2Lista();
            //ControlsVisibility(true);

            labelACarregar.Text = "Países atualizados";


            if (load) //se carregou a BD dá a data corrente
            {
                labelStatus.Text = ($"Países carregados com sucesso em " +
                    DateTime.Now.ToString("dd MMM yyyy HH:mm"));
            }
            else // se foi à BD, vai buscar a data de modificação do fich./BD
            {
                labelStatus.Text = ($"Países carregados da Base de Dados, atualizada em " +
                    File.GetLastWriteTime(@"Data\Paises.sqlite").ToString("dd MMM yyyy HH:mm"));
            }
        }


        /// <summary>
        /// Método da classe DataService através do qual são carregadas as listas, localmente, desde a DB.
        /// </summary>
        /// <remarks>
        /// Utilizadas 9 tabelas (normalização DB) para guardar a informação e 
        /// 1 lista + 8 dicionários para a receber.
        /// </remarks>
        private void LoadLocalPaises()
        {
            Paises = dataService.GetData();

        }


        /// <summary>
        /// Carrega as listas pela API, suspendendo a execução até à sua conclusão. Actualiza a DB/BD.
        /// </summary>
        /// <remarks>
        /// Apaga os dados da DB e posteriormente grava e actualiza a mesma.
        /// </remarks>
        private async Task LoadApiPaises()
        {
            var response = await apiService.GetPaises("https://restcountries.eu", "/rest/v2/all");

            Paises = (List<Country>)response.Result;

            progressBar1.Maximum = Paises.Count;   //faz equivaler os 100% da barra aos 250 países, ref utilizada durante carregamento

            dataService.DeleteData();

            dataService.SaveData(Paises, progressBar1, labelACarregar);  //envia label e progbar a fim de alterar propriedades aquando do carreg.

        }


        /// <summary>
        /// Método que 'controla' a visibilidade dos vários Controlos (Windows Forms).
        /// </summary>
        /// <param name="vsbl">Boleana de controlo de execução.</param>
        /// <returns></returns>
        private void ControlsVisibility(bool vsbl)
        {
            if (vsbl)
            {
                buttonAbout.Visible = true;
                panelInfo.Visible = true;
                labelPais.Visible = true;
                pictureBoxPin.Visible = true;
                labelLatlng.Visible = true;
                labelStatus.Visible = true;
                if (checkBoxFusos.Checked == true)
                {
                    labelTZ.Visible = true;
                    foreach (var item in panelsTZ.Zip(labelsTZ, (a, b) => new { A = a, B = b }))
                    {
                        item.A.Visible = true;
                        item.B.Visible = true;
                    }
                }

                checkBoxFronteiras.Visible = true;
                checkBoxFusos.Visible = true;
                checkBoxSearch.Visible = true;
                if (checkBoxFronteiras.Checked == true)
                {
                    for (int i = 0; i < labelBorders.Count(); i++)
                    {
                        pinBorders[i].Visible = true;
                        labelBorders[i].Visible = true;
                    }
                    labelFronteiras.Visible = true;
                }

                labelACarregar.Visible = false;
                progressBar1.Visible = false;
                
                if (checkBoxSearch.Checked == true)
                    panelSearch.Visible = true;

                comboBox1.Enabled = true;

                return;

            }

            
            
            labelFronteiras.Visible = false;
            if (labelBorders != null)
                for (int i = 0; i < labelBorders.Count(); i++)
                {
                    pinBorders[i].Visible = false;
                    labelBorders[i].Visible = false;
                }
            if (panelsTZ != null)
                foreach (var panel in panelsTZ)
                    panel.Visible = false;
            //labelStatus.Visible = false;
            labelTZ.Visible = false;
            checkBoxFronteiras.Visible = false;
            checkBoxFusos.Visible = false;
            checkBoxSearch.Visible = false;
            labelLatlng.Visible = false;
            pictureBoxPin.Visible = false;
            labelPais.Visible = false;
            panelInfo.Visible = false;
            buttonAbout.Visible = false;
            comboBox1.Enabled = false;
            panelSearch.Visible = false;

        }


        /// <summary>
        /// Ordena os vários objectos (Controlos) na Form.
        /// </summary>
        private void OrderControls()
        {
            labelTZ.BringToFront();
            if (panelsTZ != null)
                foreach (var panel in panelsTZ)
                    panel.BringToFront();
            labelUtc.BringToFront();
            pictureBoxPin.BringToFront();
            for (int i = 0; i < labelBorders.Count(); i++)
            {
                pinBorders[i].BringToFront();
                labelBorders[i].BringToFront();
            }
            panelInfo.BringToFront();
            labelPais.BringToFront();
            labelStatus.BringToFront();
            labelACarregar.BringToFront();
            comboBox1.BringToFront();
            checkBoxFusos.BringToFront();
            checkBoxFronteiras.BringToFront();
            checkBoxSearch.BringToFront();
            listView1.BringToFront();
            panelSearch.BringToFront();
            foreach (var label in labelsTZ) //mapa
                label.BringToFront();
            labelLatlng.BringToFront(); //mapa
            labelFronteiras.BringToFront(); //mapa
            panelMapa.SendToBack();
            buttonAbout.BringToFront();

        }


        /// <summary>
        /// Preenche a comboBox1 com os países, representados pelo Nome/Name.
        /// </summary>
        /// <remarks>
        /// Aciona a opção Autocomplete, com os elementos da lista.
        /// </remarks>
        public void PopulateCombo()
        {

            comboBox1.DisplayMember = "Name";
            //comboBox1.ValueMember = "value";
            comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
            comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            comboBox1.DataSource = Paises;

        }


        /// <summary>
        /// Opera uma série de ações desencadeadas por uma nova seleção de Pais, na comboBox1. 
        /// </summary>
        /// <remarks>
        /// Carrega o objecto paisB do tipo Country, com info do pais, 'carrega' a respectiva 
        /// bandeira e chama o método TravellingPin.
        /// </remarks>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            paisB = (Country)comboBox1.SelectedItem;

            ControlsVisibility(false);

            LoadInfo();

            if (load)
            {
                if (!Directory.Exists("images"))
                    Directory.CreateDirectory("images");

                byte[] imageBytes = imageService.SvgToByteArray(paisB.Flag);   // se carregou da api vai buscar os bytes da imagem Bandeira

                MemoryStream ms = new MemoryStream(imageBytes);                // cria stream em memoria atraves da classe MemoryStream, com os bytes (array)

                pictureBox1.BackgroundImage = Image.FromStream(ms, true);      // atribui à picBox a imagem através do método FromStream da classe Drawing.Image

            }
            else
            {
                pictureBox1.BackgroundImage = imageService.byteArrayToSvg(     // se não carregou atribui através da conversão desde blob na BD 
                    dataService.BlobReader(paisB.Alpha2Code));
            }

            //pictureBox1.BackgroundImage = flag;

            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox1.BackColor = Color.Green;

            TravellingPin();


            //ColumnClickEventArgs args = new ColumnClickEventArgs(0);
            if (comboBoxEscolher.SelectedItem != null)
                comboBoxEscolher_SelectedIndexChanged(sender, e);
  
        }


        /// <summary>
        /// Inicia a formatação da lista de paises e carrega a comboBox com as 
        /// propriedades-critério da classe Country.
        /// </summary>
        /// <remarks>
        /// Adiciona quatro colunas à listView1 c/ os respectivos headers.
        /// </remarks>
        private void CarregaCombo2Lista()
        {

            listView1.Columns.Add("");
            listView1.Columns.Add("PAÍS");
            listView1.Columns.Add("ÁREA");
            listView1.Columns.Add("POPULAÇÃO");

            listView1.Columns[0].Width = 1;
            listView1.Columns[1].Width = 150;

            List<string> criterios = new List<string> { "Region", "Subregion",
                "Timezones", "Currencies", "Languages" };

            foreach (var prop in typeof(Country).GetProperties())    // vai buscar as propriedades/critério e carrega a combo
            {
                string nomePropriedade = prop.Name;
                if (criterios.Contains(nomePropriedade))
                    comboBoxEscolher.Items.Add(nomePropriedade);
            }

            comboBoxEscolher.SelectedIndex = 0;

        }


        /// <summary>
        /// Recebe e apresenta, nas labels do painelInfo, grande parte da informação respetiva ao pais.
        /// </summary>
        private void LoadInfo()
        {
            labelPais.Text = "País: " + paisB.Translations.pt.ToUpper() + $" ({paisB.Alpha2Code} - {paisB.Alpha3Code})";
            labelPais.Location = new Point(185 + (panelMapa.Width - 185) / 2 - labelPais.Width / 2, 50);
            labelPais.ForeColor = Color.Silver;
            labelNative.Text = paisB.NativeName;
            labelCapital.Text = "Capital: " + paisB.Capital;
            labelRegiao.Text = "Região: " + paisB.Region;
            labelTelef.Text = "Cód.Tel.: " + paisB.CallingCodes[0];
            labelPopulacao.Text = "População: " + paisB.Population;
            labelDominio.Text = "Domínio Internet: " + paisB.TopLevelDomain[0];

            List<string> moedas = new List<string>();
            foreach (Currency crrncy in paisB.Currencies)
                moedas.Add(crrncy.Name + $" ({crrncy.Symbol})");
            labelMoeda.Text = "Moedas: " + string.Join(", ", moedas);
            List<string> linguas = new List<string>();
            foreach (Language lngge in paisB.Languages)
                linguas.Add(lngge.Name);
            labelMoeda.Text += "\n\nLínguas: " + string.Join(", ", linguas);
            //labelLingua.Location = new Point(labelPais.Right,
            //    labelPais.Bottom - labelLingua.Height);
            labelMoeda.Left = panelInfo.Width / 2 - labelMoeda.Width / 2;

            panelInfo.Height = labelMoeda.Bottom - pictureBox2.Top + 20;
        }



        /// <summary>
        /// Sequencia a maior parte dos métodos respeitantes à apresentação gráfica.
        /// </summary>
        private void TravellingPin()
        {

            CreateTZPanels();

            tzCreated = CreateTZLabels(tzCreated);

            InsertPin(pictureBoxPin, paisB.Latlng, labelLatlng, paisB.LabelLatlngTxt());

            InsertBorderPins();

            OrderControls();

            ControlsVisibility(true);
            
        }


        /// <summary>
        /// Cria, insere (posiciona) os pins ou marcadores e labels, ambos a vermelho 
        /// (através do método geral InsertPin) que indicam a localização dos países 
        /// que fazem fronteira com o país selecionado.
        /// </summary>
        /// <remarks>
        /// Através de query 'linq' são encontrados os países fronteira, processados 
        /// através de arrays de labels e pictureBoxes.
        /// Chama insertLabelFronteiras() que insere 'legenda'.
        /// </remarks>
        private void InsertBorderPins()
        {
            // criados e inseridos mini-pins dos 'países-fronteira' + respctvs. labels


            if (pinBorders != null)                                     // se já existirem pins fronteira, elimina p/ carregar novos
                for (int i = 0; i < pinBorders.Length; i++)
                {
                    //Controls.Remove(pinBorders[i]);
                    //Controls.Remove(labelBorders[i]);
                    pinBorders[i].Dispose();                
                    labelBorders[i].Dispose();
                }


            var pesquisa = from Pais in Paises                               // linq - recebe a coleção c/ os paises fronteira
                           where paisB.Borders.Contains(Pais.Alpha3Code)
                           select Pais;


            labelBorders = new Label[pesquisa.Count()];
            pinBorders = new PictureBox[pesquisa.Count()];

            List<string> paisesBord = new List<string>();

            double maxX = 0.0;
            double minX = 700;
            Point auxX;
            int n = 0;
            foreach (var pb in pesquisa)                         // executa os comandos para cada país: cria pin e label nos arrays
            {                                                    // e adiciona ao form e insere na posição - regista posi. max e min       

                pinBorders[n] = new PictureBox
                {
                    BackColor = Color.Red,
                    Size = new Size(5, 5),
                };
                Controls.Add(pinBorders[n]);
                pinBorders[n].Visible = false;

                labelBorders[n] = new Label
                {
                    BackColor = Color.Black,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("MS Sans Serif", 6, FontStyle.Bold),
                    ForeColor = Color.Red,
                    AutoSize = true,
                };
                Controls.Add(labelBorders[n]);
                labelBorders[n].Visible = false;

                InsertPin(pinBorders[n], pb.Latlng, labelBorders[n], pb.Alpha3Code);
                n++;

                paisesBord.Add(pb.Translations.pt.Length < 16 ? pb.Translations.pt : pb.Translations.pt.Substring(0, 15) + "...");

                auxX = mapService.CoordsToMapa(pb.Latlng[1], pb.Latlng[0], panelMapa.Width, Height, pictureBoxPin);
                if (auxX.X > maxX)
                    maxX = auxX.X;

                if (auxX.X < minX)
                    minX = auxX.X;
            }


            insertLabelFronteiras(paisesBord, maxX, minX);

        }



        /// <summary>
        /// Carrega a 'labelFronteiras' com os nomes dos países que fazem fronteira, que serve de legenda.
        /// </summary>
        /// <remarks>
        /// Inicialmente fazia depender a cor de fundo da inteseção ou não com as 'timezones' e 
        /// o posicionamento dependia da presença ou não de outros controlos.
        /// </remarks>
        /// <param name="paisesFronteira">recebe a lista dos países que fazem fronteira.</param>
        /// <param name="maxX">Valor da posição do pin mais avançado.</param>
        /// <param name="minX">Pin fronteira mais recuado</param>
        private void insertLabelFronteiras(List<string> paisesFronteira, double maxX, double minX)
        {

            if (paisesFronteira.Count() == 0)
            {
                labelFronteiras.Text = "";
                return;
            }

            labelFronteiras.BackColor = Color.Transparent;
            labelFronteiras.Parent = panelMapa;
            //labelFronteiras.Font = new Font(labelFronteiras.Font, labelFronteiras.Font.Style ^ FontStyle.Italic);
            labelFronteiras.Font = new Font("MS Sans Serif", 8, FontStyle.Bold);
            labelFronteiras.ForeColor = Color.Red;

            labelFronteiras.Text = "FRONTEIRAS:\n\n\u25A0  ";
            labelFronteiras.Text += string.Join("\n\u25A0  ", paisesFronteira);

            if (pictureBoxPin.Left - pictureBoxPin.Width / 2 > panelMapa.Width / 2)
            {
                if (panelInfo.Right > minX)
                {
                    panelInfo.Left = (int)minX - panelInfo.Width - 30;
                }
            }
            else
            {
                if (panelInfo.Left < maxX)
                    panelInfo.Left = (int)maxX + 30;
            }


            //labelFronteiras.Location = labelFronteiras.Left < 0 ? new Point(0, labelPais.Bottom + 30) :
            //    panelInfo.Location;

            //if (labelFronteiras.Right > panelMapa.Width)
            //{
            //    labelFronteiras.BackColor = Color.Black;
            //    labelFronteiras.Parent = this;
            //}


            //foreach (var tz in panelsTZ)
                //if (labelFronteiras.Right > tz.Left && labelFronteiras.Left < tz.Right)
                //{
            labelFronteiras.BackColor = Color.Black; //FromArgb(50, Color.White);
            labelFronteiras.Parent = this;
                    //labelFronteiras.BringToFront();
                //}

            labelFronteiras.Location = new Point(20 * panelMapa.Height / 700, 400);

        }


        /// <summary>
        /// Posiciona o 'pin' que sinaliza o país no mapa e paises fronteira, com respetivas labels
        /// com info. das coordenadas latitude/longitude ou do país fronteira.
        /// </summary>
        /// <param name="pin">É passado o pin, uma vez que este método é utilizado pela generalidade dos pins</param>
        /// <param name="ltlg">Label com os valores Latlng.</param>
        /// <param name="labelX">Labels genéricas apensas aos pins.</param>
        /// <param name="txt">Texto das labels.</param>
        private void InsertPin(PictureBox pin, List<double> ltlg, Label labelX, string txt)
        {
            
            //pictureBoxPin.SizeMode = PictureBoxSizeMode.AutoSize;
            Point ponto = new Point();
                                                                                        // vai buscar posição X e Y no form em correspondência c/ coordenadas
            if (ltlg.Count == 2)
                ponto = mapService.CoordsToMapa(ltlg[1], ltlg[0], panelMapa.Width, Height, pictureBoxPin);
            int x = ponto.X;
            int y = ponto.Y;

            pin.Location = new Point(x - pin.Width / 2, y - pin.Height / 2);

            //bool downUnder = false;
            int defaultY = 150;
            panelInfo.Top = defaultY;

            labelX.Text = txt;
                
            int maxPTZ = 0, minPTZ = 700;
            foreach (var ptz in panelsTZ)
            {
                if (maxPTZ < ptz.Right)
                    maxPTZ = ptz.Right;
                if (minPTZ > ptz.Left)
                    minPTZ = ptz.Left;
            }
            //MessageBox.Show(maxPTZ + "  " + minPTZ);

            if (pin == pictureBoxPin)
            {
                labelX.Parent = panelMapa;
                labelX.BackColor = Color.Transparent;

                if (pictureBoxPin.Location.X - pictureBoxPin.Width / 2 > panelMapa.Width / 2)               //posiciona painel info em funçao do pin que assinala o pais
                {
                    panelInfo.Location = new Point(pin.Location.X - panelInfo.Width - 50, defaultY);
                    labelX.Location = new Point(pin.Right + 10, pin.Top - (int)(1.5 * labelX.Height));
                    labelX.Left = labelX.Right > panelMapa.Width ? panelMapa.Width - labelX.Width :
                        labelX.Left < maxPTZ ? maxPTZ : labelX.Left;
                }
                else
                {
                    panelInfo.Location = new Point(pin.Location.X + pin.Width + 50, defaultY);
                    labelX.Location = new Point(pin.Left - labelX.Width - 10, pin.Top - (int)(1.5 * labelX.Height));
                    labelX.Left = labelX.Left < 0 ? 0 : labelX.Right > minPTZ && labelX.Left < minPTZ + 28 ?
                        minPTZ - labelX.Width : labelX.Left;
                }

                foreach (var tz in panelsTZ)
                    if (labelX.Right > tz.Left && labelX.Left < tz.Right)                                   // verifica interseções p/ melhorar apresentação
                    {
                        labelX.BackColor = Color.Black;
                        labelX.Parent = this;
                        //labelX.BringToFront();
                    }

                if (labelX.Right > panelMapa.Width)
                {
                    labelX.BackColor = Color.Black;
                    labelX.Parent = this;
                }
            }

            else
            {
                if (pin.Left + pin.Width / 2 > pictureBoxPin.Left + pictureBoxPin.Width / 2)
                    labelX.Location = new Point(pin.Right, pin.Top);
                else
                    labelX.Location = new Point(pin.Left - labelX.Width, pin.Top);
            }

        }



        /// <summary>
        /// Cria as labels com as referências (marcadores) das TZ (time zones), 
        /// na parte inferior do mapa. Chamado apenas uma vez.
        /// </summary>
        /// <param name="created">Boleana determina a execução apenas se ainda não foram criadas as labels</param>
        /// <returns></returns>
        private bool CreateTZLabels(bool created)
        {

            if (!created)
            {
                

                int n = 27;
                Label[] labels = new Label[n];
                for (int i = 0; i < n; i++)
                {
                    labels[i] = new Label
                    {
                        BackColor = Color.Transparent,
                        Size = new Size(panelMapa.Width / 25, 25),
                        Top = panelMapa.Height - 25,
                        TextAlign = ContentAlignment.MiddleCenter,
                        Font = new Font("MS Sans Serif", 8, FontStyle.Bold),
                        ForeColor = Color.DarkGray,
                        AutoSize = false,
                    };

                    labels[i].BorderStyle = BorderStyle.FixedSingle;
                    //labels[i].BringToFront();

                }

                int y = -12;
                for (int i = 0; i < n; i++)
                {
                    labels[i].Left = mapService.timeZoneLocX2(y, panelMapa.Width);
                    labels[i].Text = Convert.ToString(y);
                    Controls.Add(labels[i]);
                    y++;
                }

                labelTZ.Text = string.Join("\n", paisB.Timezones);
                labelTZ.Left = panelsTZ[0].Left - labelTZ.Width - 20;
                labelTZ.Left = labelTZ.Left < 0 ? panelsTZ[panelsTZ.Length - 1].Right + 20 : labelTZ.Left;
                labelTZ.Top = panelMapa.Height - 35 - labelTZ.Height;
                labelTZ.ForeColor = Color.LightGray;

                return true;
            }

            //labelTZ.Text = "Fusos Horários:\n\n";
            labelTZ.Text = string.Join("\n", paisB.Timezones);
            labelTZ.Left = panelsTZ[0].Left - labelTZ.Width - 20;
            labelTZ.Left = labelTZ.Left < 0 ? panelsTZ[panelsTZ.Length - 1].Right + 20 : labelTZ.Left;
            labelTZ.Top = panelMapa.Height - 35 - labelTZ.Height;


            return true;

        }



        /// <summary>
        /// Elimina e cria ('clona' - através do método Clone) novos paineis (referência visual de 
        /// time zone (c/ as respectivas labels c/ indicação da TZ), para cada país, recorrendo a arrays.
        /// </summary>
        private void CreateTZPanels()
        {

            if (panelsTZ != null)
                for (int i = 0; i < panelsTZ.Length; i++)
                {
                    panelsTZ[i].Dispose();
                }


            panelsTZ = new Panel[paisB.Timezones.Count];
            labelsTZ = new Label[paisB.Timezones.Count];

            for (int i = 0; i < paisB.Timezones.Count; i++)
            {

                panelsTZ[i] = new Panel();
                panelsTZ[i] = panelTZ.Clone();


                labelsTZ[i] = new Label();
                labelsTZ[i] = labelUtc.Clone();


                panelsTZ[i].Controls.Add(labelsTZ[i]);

                labelUtc.Text = "";
                                                                                            // cria e posiciona a repres. das time zones convertendo as strings UTM em inteiros

                if (paisB.Timezones[i] == "UTC")
                    panelsTZ[i].Left = mapService.timeZoneLocX("UTC 00:00", panelMapa.Width) + 19;
                else
                    panelsTZ[i].Left = mapService.timeZoneLocX(paisB.Timezones[i], panelMapa.Width) + 19;


                labelsTZ[i].Text = paisB.Timezones[i].Length == 1 ? "NA" :
                        paisB.Timezones[i].Length == 3 ? "0" :
                        paisB.Timezones[i].Length == 6 ? paisB.Timezones[i].Substring(3, 3) :
                        paisB.Timezones[i][7] == '0' ?
                        paisB.Timezones[i].Substring(3, 3) : paisB.Timezones[i].Substring(3, 3) + "\n:30";


                Controls.Add(panelsTZ[i]);


            }
        }




        /// <summary>
        /// Se o botão checkbox respectivo estiver marcado, esconde info das fronteiras.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxFronteiras_CheckedChanged(object sender, EventArgs e)
        {
            labelFronteiras.Visible = labelFronteiras.Visible == false ? true : false;
            if (labelBorders != null)
                for (int i = 0; i < labelBorders.Count(); i++)
                {
                    pinBorders[i].Visible = pinBorders[i].Visible == false ? true : false;
                    labelBorders[i].Visible = labelBorders[i].Visible == false ? true : false;
                }
        }

        /// <summary>
        /// Se o botão checkbox respectivo estiver marcado, esconde info dos fusos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxFusos_CheckedChanged(object sender, EventArgs e)
        {
            labelTZ.Visible = labelTZ.Visible == false ? true : false;
            if (labelsTZ != null)
                for (int i = 0; i < labelsTZ.Count(); i++)
                {
                    labelsTZ[i].Visible = labelsTZ[i].Visible == false ? true : false;
                    panelsTZ[i].Visible = panelsTZ[i].Visible == false ? true : false;
                }
        }

        /// <summary>
        /// Se o botão checkbox respectivo estiver marcado, esconde painel search/pesquisa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxSearch_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSearch.Checked == false)
                panelSearch.Visible = false;
            else
            {
                panelSearch.Location = new Point(215, 475);
                panelSearch.Visible = true;
            }

        }



        /// <summary>
        /// Apenas para testes, devolve os objectos da 'cena'
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public IEnumerable<Control> GetAll(Control control) //, Type type)
        {
            var controls = control.Controls.Cast<Control>();


            return controls.SelectMany(ctrl => GetAll(ctrl)) //, type))
                                      .Concat(controls);

            //.Where(c => c.GetType() == type);
        }



        /// <summary>
        /// Testes. Posição do rato em coordenadas ou pixeis.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void panel1_MouseMove(object sender, MouseEventArgs e)
        //{
        //    Point ptE = new Point(e.Location.X, e.Location.Y);
        //    ptE = PointToClient(ptE);
        //    double lng = mapService.MapaToCoords(ptE.X, ptE.Y,Width, Height, pictureBoxPin)[0];
        //    double lat = mapService.MapaToCoords(ptE.X, ptE.Y,panelMapa.Width, Height, pictureBoxPin)[1];
        //    //Text = e.Location.X + ":" + e.Location.Y;
        //    Text = string.Format("Paises   lng: {0:N2} lat: {1:N2}", lng, lat);
        //}


        /// <remarks>
        /// Utilizado para o arrastar dos paineis Search e Info, traz cada um para a frente e envia posição. 
        /// </remarks>
        void c_MouseDown(object sender, MouseEventArgs e)
        {
            Control c = sender as Control;
            c.DoDragDrop(c, DragDropEffects.Move);
            if (sender == panelInfo)
            {
                panelInfo.Parent = this;
                panelInfo.BringToFront();
            }
            else
            {
                panelSearch.Parent = this;
                panelSearch.BringToFront();
            }

        }

        /// <remarks>
        /// Posiciona os dois paineis 'draggable' dentro do painel Mapa. 
        /// </remarks>
        void panelMapa_DragDrop(object sender, DragEventArgs e)
        {
            Control c = e.Data.GetData(e.Data.GetFormats()[0]) as Control;
            if (c != null)
            {
                c.Location = panelMapa.PointToClient(new Point(e.X, e.Y));
                panelMapa.Controls.Add(c);
            }
        }

        /// <remarks>
        /// Possibilita o arrastar dos paines no mapa através do efeito da classe DragEventArgs. 
        /// </remarks>
        void panelMapa_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }



        /// <summary>
        /// Ao fazer uma nova seleção na combo, preenche a listview com elementos resultantes da pesquisa, de acordo com o critério.
        /// </summary>
        /// <remarks>
        /// Vai buscar todos os paises nas mesmas Region, Subregion e que têm moedas, línguas e fusos horários em comum.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //IEnumerable<Country> lista;
        private void comboBoxEscolher_SelectedIndexChanged(object sender, EventArgs e)
        {

            listView1.Clear();

            listView1.Columns.Add("");
            listView1.Columns.Add("PAÍS");
            listView1.Columns.Add("ÁREA");
            listView1.Columns.Add("POPULAÇÃO");

            listView1.Columns[0].Width = 1;
            listView1.Columns[1].Width = 150;

            
            var lista = comboBoxEscolher.SelectedItem.ToString() == "Region" ?                          // linq primeiras queries correspondentes aos criterios
                from Pais in Paises where Pais.Region == paisB.Region select Pais:
                
                comboBoxEscolher.SelectedItem.ToString() == "Subregion" ?
                from Pais in Paises where Pais.Subregion == paisB.Subregion select Pais :

                comboBoxEscolher.SelectedItem.ToString() == "Timezones" ?
                from Pais in Paises where Pais.Timezones[0] == paisB.Timezones[0] select Pais :

                comboBoxEscolher.SelectedItem.ToString() == "Currencies" ?
                from Pais in Paises where Pais.Currencies[0].Name == paisB.Currencies[0].Name select Pais :
                
                from Pais in Paises where Pais.Languages[0].Name == paisB.Languages[0].Name select Pais;


            ////MessageBox.Show(string.Join("*", listaTimezones.ToList()));


            if (comboBoxEscolher.SelectedItem.ToString() == "Timezones")                                   // refinamento das queries para ir buscar todos os 
                lista = lista.Concat(from Pais in Paises                                                   // paises com timezones, linguas e moedas coincidentes
                                    where Pais.Timezones.Intersect(paisB.Timezones).Count() > 0            // mesmo 2, 3 linguas, etc.
                                    select Pais);


            if (comboBoxEscolher.SelectedItem.ToString() == "Languages")
            {
                for (int i = 0; i < paisB.Languages.Count; i++)
                    lista = lista.Concat(Paises.Where(x => x.Languages
                        .Where(y => y.Name == paisB.Languages[i].Name).Count() > 0).ToList());
            }


            if (comboBoxEscolher.SelectedItem.ToString() == "Currencies")
                for (int i = 0; i < paisB.Currencies.Count - 1; i++)
                    lista = lista.Concat(Paises.Where(x => x.Currencies
                        .Where(y => y.Name == paisB.Currencies[i].Name).Count() > 0).ToList());


            lista = lista.Distinct();                                                                       // elimina duplicados p/ adicionar a list view


            List<string> criterios = new List<string>{ "Region", "Subregion" };
            //var propr = GetType().GetProperty(lista.FirstOrDefault().ToString());

            if (criterios.Contains(comboBoxEscolher.SelectedItem.ToString()))
                labelEscolher.Text = comboBoxEscolher.SelectedItem.ToString() == "Region" ?
                    $"Region {paisB.Region.ToString()}: ({lista.Count()})" : $"Subregion {paisB.Subregion.ToString()}: ({lista.Count()})";
            else
                labelEscolher.Text = $"Same {comboBoxEscolher.SelectedItem.ToString().ToLower()}: ({lista.Count()})";



            foreach (var paiz in lista)
            {
                ListViewItem item = new ListViewItem();
                item.Tag = paiz;

                item = listView1.Items.Add(item);
                item.SubItems.Add(paiz.Translations.pt);
                item.SubItems.Add(paiz.Area.ToString());
                item.SubItems.Add(paiz.Population.ToString());
                
            }

            

            for (int idx = 2; idx <= 3; idx++)
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }

        }


        /// <remark>
        /// 'Fecha' o painel das pesquisas, através da propriedade de visibilidade do controlo.
        /// </remark>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelFechar_Click(object sender, EventArgs e)
        {
            checkBoxSearch.Checked = false;
            panelSearch.Visible = false;

        }


        /// <summary>
        /// Processa os ordenamentos pelos campos da lista (click headers) das pesquisas por ordem ascendente/descendente.
        /// </summary>
        /// <remarks>
        /// Utilizada instância de uma subclasse da IComparer, onde é filtrada e organizada a lista
        /// por valor alfabético ou numérico.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {


            if (e.Column != sortColumn)
            {
                sortColumn = e.Column;
                listView1.Sorting = SortOrder.Ascending;
            }
            else
            {
                if (listView1.Sorting == SortOrder.Ascending)
                    listView1.Sorting = SortOrder.Descending;
                else
                    listView1.Sorting = SortOrder.Ascending;
            }

            listView1.Sort();

            ListViewItemCollection lvic = new ListViewItemCollection(listView1); 
            nListVItems = lvic.Count;

            listView1.ListViewItemSorter =
                new ListViewItemComparer(e.Column, listView1.Sorting, nListVItems, listView1);

   
        }

        /// <summary>
        /// Permite que seja selecionado o Pais através da listView1.
        /// </summary>
        /// <remarks>
        /// ...Passado para a comboBox1 de 'controlo' dos Paises através da tag 
        /// do item, que guarda o País na coluna de indice 0, 'invisivel'.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            var item = listView1.SelectedItems[0];
            paisB = (Country)item.Tag;
            //MessageBox.Show(item.Tag.ToString());
            comboBox1.SelectedIndex = comboBox1.Items.IndexOf(paisB);

        }


        /// <remark>
        /// Mostra janela com a informação dos créditos, versão e descrição da aplicação.
        /// </remark>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAbout_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutBox1 = new AboutBox1();
            aboutBox1.Show();
        }
    }
}
