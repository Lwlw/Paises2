﻿namespace Paises
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param Name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelPopulacao = new System.Windows.Forms.Label();
            this.labelMoeda = new System.Windows.Forms.Label();
            this.labelDominio = new System.Windows.Forms.Label();
            this.labelTelef = new System.Windows.Forms.Label();
            this.labelRegiao = new System.Windows.Forms.Label();
            this.labelCapital = new System.Windows.Forms.Label();
            this.labelNative = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelPais = new System.Windows.Forms.Label();
            this.labelTZ = new System.Windows.Forms.Label();
            this.panelTZ = new System.Windows.Forms.Panel();
            this.labelUtc = new System.Windows.Forms.Label();
            this.labelLatlng = new System.Windows.Forms.Label();
            this.labelACarregar = new System.Windows.Forms.Label();
            this.pictureBoxPin = new System.Windows.Forms.PictureBox();
            this.labelFronteiras = new System.Windows.Forms.Label();
            this.pictureBoxBorder = new System.Windows.Forms.PictureBox();
            this.labelBorder = new System.Windows.Forms.Label();
            this.checkBoxFronteiras = new System.Windows.Forms.CheckBox();
            this.checkBoxFusos = new System.Windows.Forms.CheckBox();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.labelFechar = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.comboBoxEscolher = new System.Windows.Forms.ComboBox();
            this.labelEscolher = new System.Windows.Forms.Label();
            this.panelMapa = new System.Windows.Forms.Panel();
            this.labelStatus = new System.Windows.Forms.Label();
            this.checkBoxSearch = new System.Windows.Forms.CheckBox();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.panelInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelTZ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBorder)).BeginInit();
            this.panelSearch.SuspendLayout();
            this.panelMapa.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.Black;
            this.comboBox1.DropDownWidth = 200;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.White;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(16, 22);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(183, 23);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Black;
            this.progressBar1.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.progressBar1.Location = new System.Drawing.Point(279, 558);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(273, 21);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 5;
            // 
            // panelInfo
            // 
            this.panelInfo.BackColor = System.Drawing.Color.Transparent;
            this.panelInfo.BackgroundImage = global::Paises.Properties.Resources.barraInfo;
            this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelInfo.Controls.Add(this.labelPopulacao);
            this.panelInfo.Controls.Add(this.labelMoeda);
            this.panelInfo.Controls.Add(this.labelDominio);
            this.panelInfo.Controls.Add(this.labelTelef);
            this.panelInfo.Controls.Add(this.labelRegiao);
            this.panelInfo.Controls.Add(this.labelCapital);
            this.panelInfo.Controls.Add(this.labelNative);
            this.panelInfo.Controls.Add(this.pictureBox1);
            this.panelInfo.Controls.Add(this.pictureBox2);
            this.panelInfo.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.panelInfo.Location = new System.Drawing.Point(718, 99);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(185, 291);
            this.panelInfo.TabIndex = 6;
            this.panelInfo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.c_MouseDown);
            // 
            // labelPopulacao
            // 
            this.labelPopulacao.BackColor = System.Drawing.Color.Transparent;
            this.labelPopulacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPopulacao.ForeColor = System.Drawing.Color.Black;
            this.labelPopulacao.Location = new System.Drawing.Point(0, 161);
            this.labelPopulacao.Name = "labelPopulacao";
            this.labelPopulacao.Size = new System.Drawing.Size(185, 25);
            this.labelPopulacao.TabIndex = 17;
            this.labelPopulacao.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelMoeda
            // 
            this.labelMoeda.AutoSize = true;
            this.labelMoeda.BackColor = System.Drawing.Color.Transparent;
            this.labelMoeda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMoeda.ForeColor = System.Drawing.Color.Black;
            this.labelMoeda.Location = new System.Drawing.Point(0, 228);
            this.labelMoeda.MaximumSize = new System.Drawing.Size(185, 0);
            this.labelMoeda.Name = "labelMoeda";
            this.labelMoeda.Size = new System.Drawing.Size(0, 13);
            this.labelMoeda.TabIndex = 11;
            this.labelMoeda.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelDominio
            // 
            this.labelDominio.BackColor = System.Drawing.Color.Transparent;
            this.labelDominio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDominio.ForeColor = System.Drawing.Color.Black;
            this.labelDominio.Location = new System.Drawing.Point(1, 206);
            this.labelDominio.Name = "labelDominio";
            this.labelDominio.Size = new System.Drawing.Size(185, 25);
            this.labelDominio.TabIndex = 12;
            this.labelDominio.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTelef
            // 
            this.labelTelef.BackColor = System.Drawing.Color.Transparent;
            this.labelTelef.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTelef.ForeColor = System.Drawing.Color.Black;
            this.labelTelef.Location = new System.Drawing.Point(0, 184);
            this.labelTelef.Name = "labelTelef";
            this.labelTelef.Size = new System.Drawing.Size(185, 25);
            this.labelTelef.TabIndex = 16;
            this.labelTelef.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelRegiao
            // 
            this.labelRegiao.BackColor = System.Drawing.Color.Transparent;
            this.labelRegiao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegiao.ForeColor = System.Drawing.Color.Black;
            this.labelRegiao.Location = new System.Drawing.Point(0, 138);
            this.labelRegiao.Name = "labelRegiao";
            this.labelRegiao.Size = new System.Drawing.Size(185, 25);
            this.labelRegiao.TabIndex = 9;
            this.labelRegiao.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelCapital
            // 
            this.labelCapital.BackColor = System.Drawing.Color.Transparent;
            this.labelCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCapital.ForeColor = System.Drawing.Color.Black;
            this.labelCapital.Location = new System.Drawing.Point(0, 115);
            this.labelCapital.Name = "labelCapital";
            this.labelCapital.Size = new System.Drawing.Size(185, 25);
            this.labelCapital.TabIndex = 8;
            this.labelCapital.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelNative
            // 
            this.labelNative.BackColor = System.Drawing.Color.Transparent;
            this.labelNative.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNative.ForeColor = System.Drawing.Color.White;
            this.labelNative.Location = new System.Drawing.Point(0, 89);
            this.labelNative.Name = "labelNative";
            this.labelNative.Size = new System.Drawing.Size(185, 17);
            this.labelNative.TabIndex = 15;
            this.labelNative.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(44, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(98, 70);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(42, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(102, 74);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // labelPais
            // 
            this.labelPais.AutoSize = true;
            this.labelPais.BackColor = System.Drawing.Color.Transparent;
            this.labelPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPais.ForeColor = System.Drawing.Color.LightGray;
            this.labelPais.Location = new System.Drawing.Point(737, 540);
            this.labelPais.Name = "labelPais";
            this.labelPais.Size = new System.Drawing.Size(0, 18);
            this.labelPais.TabIndex = 7;
            this.labelPais.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTZ
            // 
            this.labelTZ.AutoSize = true;
            this.labelTZ.BackColor = System.Drawing.Color.Transparent;
            this.labelTZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTZ.ForeColor = System.Drawing.Color.White;
            this.labelTZ.Location = new System.Drawing.Point(276, 615);
            this.labelTZ.Name = "labelTZ";
            this.labelTZ.Size = new System.Drawing.Size(0, 13);
            this.labelTZ.TabIndex = 10;
            // 
            // panelTZ
            // 
            this.panelTZ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelTZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTZ.Controls.Add(this.labelUtc);
            this.panelTZ.Location = new System.Drawing.Point(456, 0);
            this.panelTZ.Name = "panelTZ";
            this.panelTZ.Size = new System.Drawing.Size(28, 700);
            this.panelTZ.TabIndex = 11;
            this.panelTZ.Visible = false;
            // 
            // labelUtc
            // 
            this.labelUtc.BackColor = System.Drawing.Color.Transparent;
            this.labelUtc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUtc.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.labelUtc.Location = new System.Drawing.Point(-7, 660);
            this.labelUtc.Margin = new System.Windows.Forms.Padding(0);
            this.labelUtc.Name = "labelUtc";
            this.labelUtc.Size = new System.Drawing.Size(40, 35);
            this.labelUtc.TabIndex = 0;
            this.labelUtc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLatlng
            // 
            this.labelLatlng.AutoSize = true;
            this.labelLatlng.BackColor = System.Drawing.Color.Transparent;
            this.labelLatlng.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLatlng.ForeColor = System.Drawing.Color.Turquoise;
            this.labelLatlng.Location = new System.Drawing.Point(729, 540);
            this.labelLatlng.Name = "labelLatlng";
            this.labelLatlng.Size = new System.Drawing.Size(0, 15);
            this.labelLatlng.TabIndex = 12;
            // 
            // labelACarregar
            // 
            this.labelACarregar.BackColor = System.Drawing.Color.Transparent;
            this.labelACarregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelACarregar.ForeColor = System.Drawing.Color.DarkGray;
            this.labelACarregar.Location = new System.Drawing.Point(276, 584);
            this.labelACarregar.Name = "labelACarregar";
            this.labelACarregar.Size = new System.Drawing.Size(276, 23);
            this.labelACarregar.TabIndex = 17;
            this.labelACarregar.Text = "Resultado...";
            // 
            // pictureBoxPin
            // 
            this.pictureBoxPin.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxPin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPin.Location = new System.Drawing.Point(715, 611);
            this.pictureBoxPin.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxPin.Name = "pictureBoxPin";
            this.pictureBoxPin.Size = new System.Drawing.Size(22, 22);
            this.pictureBoxPin.TabIndex = 7;
            this.pictureBoxPin.TabStop = false;
            // 
            // labelFronteiras
            // 
            this.labelFronteiras.AutoSize = true;
            this.labelFronteiras.BackColor = System.Drawing.Color.Transparent;
            this.labelFronteiras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFronteiras.ForeColor = System.Drawing.Color.Red;
            this.labelFronteiras.Location = new System.Drawing.Point(715, 543);
            this.labelFronteiras.Name = "labelFronteiras";
            this.labelFronteiras.Padding = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.labelFronteiras.Size = new System.Drawing.Size(6, 24);
            this.labelFronteiras.TabIndex = 22;
            // 
            // pictureBoxBorder
            // 
            this.pictureBoxBorder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxBorder.Location = new System.Drawing.Point(715, 579);
            this.pictureBoxBorder.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxBorder.Name = "pictureBoxBorder";
            this.pictureBoxBorder.Size = new System.Drawing.Size(5, 5);
            this.pictureBoxBorder.TabIndex = 20;
            this.pictureBoxBorder.TabStop = false;
            // 
            // labelBorder
            // 
            this.labelBorder.AutoSize = true;
            this.labelBorder.BackColor = System.Drawing.Color.Black;
            this.labelBorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBorder.ForeColor = System.Drawing.Color.Red;
            this.labelBorder.Location = new System.Drawing.Point(726, 580);
            this.labelBorder.Name = "labelBorder";
            this.labelBorder.Size = new System.Drawing.Size(0, 9);
            this.labelBorder.TabIndex = 21;
            // 
            // checkBoxFronteiras
            // 
            this.checkBoxFronteiras.AutoSize = true;
            this.checkBoxFronteiras.BackColor = System.Drawing.Color.Black;
            this.checkBoxFronteiras.Checked = true;
            this.checkBoxFronteiras.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFronteiras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFronteiras.ForeColor = System.Drawing.Color.Gray;
            this.checkBoxFronteiras.Location = new System.Drawing.Point(761, 601);
            this.checkBoxFronteiras.Name = "checkBoxFronteiras";
            this.checkBoxFronteiras.Size = new System.Drawing.Size(82, 17);
            this.checkBoxFronteiras.TabIndex = 24;
            this.checkBoxFronteiras.Text = "Fronteiras";
            this.checkBoxFronteiras.UseVisualStyleBackColor = false;
            this.checkBoxFronteiras.CheckedChanged += new System.EventHandler(this.checkBoxFronteiras_CheckedChanged);
            // 
            // checkBoxFusos
            // 
            this.checkBoxFusos.AutoSize = true;
            this.checkBoxFusos.BackColor = System.Drawing.Color.Black;
            this.checkBoxFusos.Checked = true;
            this.checkBoxFusos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFusos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFusos.ForeColor = System.Drawing.Color.Gray;
            this.checkBoxFusos.Location = new System.Drawing.Point(761, 624);
            this.checkBoxFusos.Name = "checkBoxFusos";
            this.checkBoxFusos.Size = new System.Drawing.Size(59, 17);
            this.checkBoxFusos.TabIndex = 25;
            this.checkBoxFusos.Text = "Fusos";
            this.checkBoxFusos.UseVisualStyleBackColor = false;
            this.checkBoxFusos.CheckedChanged += new System.EventHandler(this.checkBoxFusos_CheckedChanged);
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.Transparent;
            this.panelSearch.BackgroundImage = global::Paises.Properties.Resources.barra26;
            this.panelSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelSearch.Controls.Add(this.labelFechar);
            this.panelSearch.Controls.Add(this.listView1);
            this.panelSearch.Controls.Add(this.comboBoxEscolher);
            this.panelSearch.Controls.Add(this.labelEscolher);
            this.panelSearch.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.panelSearch.Location = new System.Drawing.Point(215, 472);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(368, 161);
            this.panelSearch.TabIndex = 0;
            this.panelSearch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.c_MouseDown);
            // 
            // labelFechar
            // 
            this.labelFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelFechar.Location = new System.Drawing.Point(345, 2);
            this.labelFechar.Name = "labelFechar";
            this.labelFechar.Size = new System.Drawing.Size(18, 18);
            this.labelFechar.TabIndex = 3;
            this.labelFechar.Click += new System.EventHandler(this.labelFechar_Click);
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.Black;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.ForeColor = System.Drawing.Color.DimGray;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(5, 44);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(357, 112);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // comboBoxEscolher
            // 
            this.comboBoxEscolher.BackColor = System.Drawing.Color.Black;
            this.comboBoxEscolher.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxEscolher.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEscolher.ForeColor = System.Drawing.Color.White;
            this.comboBoxEscolher.FormattingEnabled = true;
            this.comboBoxEscolher.Location = new System.Drawing.Point(216, 23);
            this.comboBoxEscolher.Name = "comboBoxEscolher";
            this.comboBoxEscolher.Size = new System.Drawing.Size(146, 21);
            this.comboBoxEscolher.TabIndex = 1;
            this.comboBoxEscolher.SelectedIndexChanged += new System.EventHandler(this.comboBoxEscolher_SelectedIndexChanged);
            // 
            // labelEscolher
            // 
            this.labelEscolher.BackColor = System.Drawing.Color.Black;
            this.labelEscolher.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelEscolher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEscolher.ForeColor = System.Drawing.Color.Silver;
            this.labelEscolher.Location = new System.Drawing.Point(5, 24);
            this.labelEscolher.Name = "labelEscolher";
            this.labelEscolher.Size = new System.Drawing.Size(357, 21);
            this.labelEscolher.TabIndex = 2;
            this.labelEscolher.Text = "Critério";
            // 
            // panelMapa
            // 
            this.panelMapa.BackgroundImage = global::Paises.Properties.Resources.mapaMundiSqrsMercator;
            this.panelMapa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelMapa.Controls.Add(this.panelSearch);
            this.panelMapa.Location = new System.Drawing.Point(0, 0);
            this.panelMapa.Margin = new System.Windows.Forms.Padding(0);
            this.panelMapa.Name = "panelMapa";
            this.panelMapa.Size = new System.Drawing.Size(700, 700);
            this.panelMapa.TabIndex = 18;
            // 
            // labelStatus
            // 
            this.labelStatus.BackColor = System.Drawing.Color.Black;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.Gray;
            this.labelStatus.Location = new System.Drawing.Point(0, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Padding = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.labelStatus.Size = new System.Drawing.Size(924, 22);
            this.labelStatus.TabIndex = 9;
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxSearch
            // 
            this.checkBoxSearch.AutoSize = true;
            this.checkBoxSearch.BackColor = System.Drawing.Color.Black;
            this.checkBoxSearch.Checked = true;
            this.checkBoxSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSearch.ForeColor = System.Drawing.Color.Gray;
            this.checkBoxSearch.Location = new System.Drawing.Point(761, 575);
            this.checkBoxSearch.Name = "checkBoxSearch";
            this.checkBoxSearch.Size = new System.Drawing.Size(74, 17);
            this.checkBoxSearch.TabIndex = 26;
            this.checkBoxSearch.Text = "Procurar";
            this.checkBoxSearch.UseVisualStyleBackColor = false;
            this.checkBoxSearch.CheckedChanged += new System.EventHandler(this.checkBoxSearch_CheckedChanged);
            // 
            // buttonAbout
            // 
            this.buttonAbout.BackColor = System.Drawing.Color.Black;
            this.buttonAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAbout.ForeColor = System.Drawing.Color.Gray;
            this.buttonAbout.Location = new System.Drawing.Point(782, 675);
            this.buttonAbout.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(61, 25);
            this.buttonAbout.TabIndex = 27;
            this.buttonAbout.Text = "About";
            this.buttonAbout.UseVisualStyleBackColor = false;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(924, 700);
            this.Controls.Add(this.buttonAbout);
            this.Controls.Add(this.checkBoxSearch);
            this.Controls.Add(this.checkBoxFusos);
            this.Controls.Add(this.checkBoxFronteiras);
            this.Controls.Add(this.panelInfo);
            this.Controls.Add(this.labelLatlng);
            this.Controls.Add(this.labelFronteiras);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelBorder);
            this.Controls.Add(this.pictureBoxBorder);
            this.Controls.Add(this.labelPais);
            this.Controls.Add(this.labelTZ);
            this.Controls.Add(this.pictureBoxPin);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panelTZ);
            this.Controls.Add(this.labelACarregar);
            this.Controls.Add(this.panelMapa);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(940, 739);
            this.MinimumSize = new System.Drawing.Size(716, 739);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.panelMapa_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.panelMapa_DragOver);
            this.panelInfo.ResumeLayout(false);
            this.panelInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelTZ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBorder)).EndInit();
            this.panelSearch.ResumeLayout(false);
            this.panelMapa.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelPais;
        private System.Windows.Forms.Label labelCapital;
        private System.Windows.Forms.Label labelRegiao;
        private System.Windows.Forms.Label labelMoeda;
        private System.Windows.Forms.Label labelDominio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxPin;
        private System.Windows.Forms.Label labelNative;
        private System.Windows.Forms.Label labelTZ;
        private System.Windows.Forms.Panel panelTZ;
        private System.Windows.Forms.Label labelLatlng;
        private System.Windows.Forms.Label labelUtc;
        private System.Windows.Forms.Label labelACarregar;
        private System.Windows.Forms.PictureBox pictureBoxBorder;
        private System.Windows.Forms.Label labelBorder;
        private System.Windows.Forms.Label labelFronteiras;
        private System.Windows.Forms.Label labelTelef;
        private System.Windows.Forms.CheckBox checkBoxFronteiras;
        private System.Windows.Forms.CheckBox checkBoxFusos;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ComboBox comboBoxEscolher;
        private System.Windows.Forms.Label labelEscolher;
        private System.Windows.Forms.Panel panelMapa;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.CheckBox checkBoxSearch;
        private System.Windows.Forms.Label labelFechar;
        private System.Windows.Forms.Label labelPopulacao;
        private System.Windows.Forms.Button buttonAbout;
    }
}

