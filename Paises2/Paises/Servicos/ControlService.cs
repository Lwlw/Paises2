﻿namespace Paises.Servicos
{
    using System;
    using System.Reflection;
    using System.Windows.Forms;

    public static class ControlService
    {
        /// <summary>
        /// Faz a clonagem de 'controlos' do Windows Forms e suas propriedades. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="controlToClone">Objecto a clonar.</param>
        /// <returns>Devolve a cópia do objecto (clone)</returns>
        public static T Clone<T>(this T controlToClone)
            where T : Control
        {
            PropertyInfo[] controlProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            T instance = Activator.CreateInstance<T>();

            foreach (PropertyInfo propInfo in controlProperties)
            {
                if (propInfo.CanWrite)
                {
                    if (propInfo.Name != "WindowTarget")
                        propInfo.SetValue(instance, propInfo.GetValue(controlToClone, null), null);
                }
            }

            return instance;
        }
    }
}
