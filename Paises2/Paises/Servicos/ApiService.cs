﻿namespace Paises.Servicos
{
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    public class ApiService
    {
        /// <summary>
        /// Testa a ligação à internet e carrega o conteúdo da API
        /// em formato Json, descompactado/convertido e recebido na lista paises
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="controller"></param>
        /// <returns>Devolve os dados armazenados em paises</returns>
        public async Task<Response> GetPaises(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.GetAsync(controller);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var paises = JsonConvert.DeserializeObject<List<Country>>(result);
                return new Response
                {
                    IsSuccess = true,
                    Result = paises
                };

            }
            catch(Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };

            }

        }
    }
}
