﻿using System;
using System.Collections;
using System.Windows.Forms;
using static System.Windows.Forms.ListView;

public class ListViewItemComparer : IComparer
{
    ListView lixt;
    private int nLv;
    private int col;
    private SortOrder order;


    public ListViewItemComparer()
    {
        col = 0;
        order = SortOrder.Ascending;
    }


    public ListViewItemComparer(int column, SortOrder order, int nLVItems, ListView list)
    {
        lixt = list;
        nLv = nLVItems;
        col = column;
        this.order = order;
    }


    public int Compare(object x, object y)
    {
        if (!(x is ListViewItem))
            return 0;
        if (!(y is ListViewItem))
            return 0;

        var l1 = (ListViewItem)x;
        var l2 = (ListViewItem)y;

        var value1 = 0.0;
        var value2 = 0.0;

        ListViewItemCollection lvic = new ListViewItemCollection(lixt);
        int n = lvic.Count;

        if (n != nLv)
        {
            return 0;
        }
        else
            try
            {

                if (Double.TryParse(l1.SubItems[col].Text, out value1) &&
                    Double.TryParse(l2.SubItems[col].Text, out value2))
                {
                    if (order == SortOrder.Ascending)
                    {
                        return value1.CompareTo(value2);
                    }
                    else
                    {
                        return value2.CompareTo(value1);
                    }
                }
                else
                {
                    var str1 = l1.SubItems[col].Text;
                    var str2 = l2.SubItems[col].Text;

                    if (order == SortOrder.Ascending)
                    {
                        return str1.CompareTo(str2);
                    }
                    else
                    {
                        return str2.CompareTo(str1);
                    }
                }
            }
            catch
            {
                return 0;
            }

    }

}