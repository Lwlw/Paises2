﻿namespace Paises.Servicos
{
    using Paises.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SQLite;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    public class DataService
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogService dialogService;
        private ImageService imageService;


        /// <summary>
        /// Método 'construtor', cria/abre a base de dados Sqlite Paises na diretoria especificada Data 
        /// (se não houver, cria a pasta, na diretoria do executável)
        /// </summary>
        /// <remarks>
        /// Criada uma ligação e comando sqlite/sql através das classes / metodos SQLite e executado o comando na BD criada.
        /// BD normalizada com criação de 9 tabelas. Uma principal para a classe Country e as outras oito para listas.
        /// </remarks>
        public DataService()
        {
            //Criação da base de dados

            dialogService = new DialogService();

            //Criar a pasta Data
            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            var path = @"Data\Paises.sqlite";

            try
            {
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open(); //Abre ou cria se a BD não existir

                //Cria as tabelas Countries, TopLevelDomain, CallingCodes, Latlng, Timezones, Borders, Currencies e Languages
                string sqlcomand =
                    "create table if not exists Countries(Name varchar(100), Alpha2Code varchar(2), Alpha3Code varchar(2), " +
                    "Capital varchar(50), Region varchar(20), Subregion varchar(50), Population int, Area real, NativeName varchar(100), " +
                    "FlagBytes blob);" +
                    "create table if not exists TopLevelDomain(Domain varchar(5), Alpha2Code varchar(2));" +
                    "create table if not exists CallingCodes(Code varchar(10), Alpha2Code varchar(2));" +
                    "create table if not exists Latlng(Lat real, Lng real, Alpha2Code varchar(2));" +
                    "create table if not exists Timezones(Timezone varchar(10), Alpha2Code varchar(2));" +
                    "create table if not exists Borders(Border varchar(10), Alpha2Code varchar(2));" +
                    "create table if not exists Currencies(Name varchar(30), Code varchar(5), Symbol varchar(5), Alpha2Code varchar(2));" +
                    "create table if not exists Languages(Name varchar(30), Alpha2Code varchar(2));" +
                    "create table if not exists Translations(pt varchar(100), Alpha2Code varchar(2));";
                    //"create table if not exists DateDBUpdate(Date varchar(30))";


                command = new SQLiteCommand(sqlcomand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }


        /// <summary>
        /// Insere os valores nas tabelas da BD Países a partir da lista Paises.
        /// </summary>
        /// <remarks>
        /// É enviada igualmente a progressbar e uma label, através das quais é feito
        /// o acompanhamento do carregamento dos dados. A gravação das imagens em bytes na
        /// tabela torna a BD mais pesada e o carregamento inicial um pouco mais moroso.
        /// </remarks>
        /// <param name="Countries"></param>
        /// <param name="progressBar1"></param>
        /// <param name="lbl"></param>
        public void SaveData(List<Country> Countries, ProgressBar progressBar1, Label lbl)
        {
            List<double> media = new List<double>();
            try
            {
                foreach (var country in Countries)
                {
                    //INFO a carregar países...
                    lbl.Text = "A carregar:                             ";
                    lbl.Update();
                    lbl.Text = "A carregar: "  + country.Name;
                    if (lbl.Text.Count() > 35) lbl.Text = lbl.Text.Remove(35) + "...";
                    lbl.Refresh();

                    //escapar as pelicas -> no longer needed
                    //if (country.Name.Contains("'"))
                    //{
                    //    int index = country.Name.IndexOf("'");
                    //    country.Name = country.Name.Insert(index, "'");
                    //}


                    imageService = new ImageService();
                    SQLiteCommand command = connection.CreateCommand();


                    //command.CommandText =
                    //    string.Format("insert into Countries (Name, Alpha2Code, Flag) " +
                    //    "values ('{0}','{1}', @0)", 
                    //    country.Name, country.Alpha2Code);

                    command.CommandText = "INSERT INTO Countries (Name, Alpha2Code, Alpha3Code, Capital, Region, Subregion, Population, " +
                        "Area, NativeName, FlagBytes) " +
                        "VALUES(@Name, @Alpha2Code, @Alpha3Code, @Capital, @Region, @Subregion, @Population, @Area, @NativeName,@FlagBytes)";

                    command.CommandType = CommandType.Text;
                    command.Parameters.Add(new SQLiteParameter("@Name", value: country.Name));
                    command.Parameters.Add(new SQLiteParameter("@Alpha2Code", value: country.Alpha2Code));
                    command.Parameters.Add(new SQLiteParameter("@Alpha3Code", value: country.Alpha3Code));
                    command.Parameters.Add(new SQLiteParameter("@Capital", value: country.Capital));
                    command.Parameters.Add(new SQLiteParameter("@Region", value: country.Region));
                    command.Parameters.Add(new SQLiteParameter("@Subregion", value: country.Subregion));
                    command.Parameters.Add(new SQLiteParameter("@Population", value: country.Population));
                    command.Parameters.Add(new SQLiteParameter("@Area", value: country.Area));
                    command.Parameters.Add(new SQLiteParameter("@NativeName", value: country.NativeName));
                    

                    try
                    {
                        SQLiteParameter parameter = new SQLiteParameter("@FlagBytes", DbType.Binary);
                        parameter.Value = imageService.SvgToByteArray(country.Flag);
                        command.Parameters.Add(parameter);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }

                    //Executar o comando
                    //command = new SQLiteCommand(sql, connection);
                    command.ExecuteNonQuery();



                    for (int i = 0; i < country.TopLevelDomain.Count(); i++)
                    {
                        command.CommandText =
                        string.Format("insert into TopLevelDomain (Domain, Alpha2Code) " +
                            "values ('{0}','{1}');",
                            country.TopLevelDomain[i], country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }


                    for (int i = 0; i < country.CallingCodes.Count(); i++)
                    {
                        command.CommandText =
                        string.Format("insert into CallingCodes (Code, Alpha2Code) " +
                            "values ('{0}','{1}');",
                            country.CallingCodes[i], country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }



                    if (country.Latlng.Count > 0)
                    {
                        command.CommandText =
                        string.Format("insert into Latlng (Lat, Lng, Alpha2Code) " +
                            "values ({0}, {1}, '{2}');",
                            country.Latlng[0], country.Latlng[1], country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        command.CommandText =
                        string.Format("insert into Latlng (Lat, Lng, Alpha2Code) " +
                            "values ({0}, {1}, '{2}');",
                            0, 0, country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }


                    for (int i = 0; i < country.Timezones.Count(); i++)
                    {
                        command.CommandText =
                        string.Format("insert into Timezones (Timezone, Alpha2Code) " +
                            "values ('{0}','{1}');",
                            country.Timezones[i], country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }

                    var limiteBrdr = country.Borders.Count() == 0 ? 1 : country.Borders.Count();
                    for (int i = 0; i < limiteBrdr; i++)
                    {
                        if (country.Borders.Count() == 0)
                            command.CommandText =
                            string.Format("insert into Borders (Border, Alpha2Code) " +
                                "values ('{0}','{1}');",
                                "", country.Alpha2Code);
                        else
                            command.CommandText =
                            string.Format("insert into Borders (Border, Alpha2Code) " +
                                "values ('{0}','{1}');",
                                country.Borders[i], country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }


                    for (int i = 0; i < country.Currencies.Count(); i++)
                    {

                        command.CommandText = "INSERT INTO Currencies (Name, Code, Symbol, Alpha2Code) " +
                        "VALUES(@Name, @Code, @Symbol, @Alpha2Code)";
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(new SQLiteParameter("@Name", value: country.Currencies[i].Name));
                        command.Parameters.Add(new SQLiteParameter("@Code", value: country.Currencies[i].code));
                        command.Parameters.Add(new SQLiteParameter("@Symbol", value: country.Currencies[i].Symbol));
                        command.Parameters.Add(new SQLiteParameter("@Alpha2Code", value: country.Alpha2Code));

                        command.ExecuteNonQuery();
                    }


                    for (int i = 0; i < country.Languages.Count(); i++)
                    {
                        command.CommandText =
                        string.Format("insert into Languages (Name, Alpha2Code) " +
                            "values ('{0}','{1}');",
                            country.Languages[i].Name, country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }


                    for (int i = 0; i < 1; i++)
                    {
                        command.CommandText =
                        string.Format("insert into Translations (pt, Alpha2Code) " +
                            "values ('{0}','{1}');",
                            country.Translations.pt, country.Alpha2Code);

                        command.ExecuteNonQuery();
                    }


                    progressBar1.Value += 1;

                }


                //command.CommandText =
                //string.Format("insert into DateDBUpdate(Date) " +
                //    "values ('{0}');",
                //    $"{DateTime.Now.ToString("ddd, dd MMM yyyy HH:mm: ss z", new CultureInfo("PT-pt"))}");

                //command.ExecuteNonQuery();


                progressBar1.Visible = false;
                //Fechar a conecção
                connection.Close();

            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }



        /// <summary>
        /// Recebe os dados da BD.
        /// </summary>
        /// <remarks>
        ///  Processamento feito através de um lista do tipo Country e de 8 dicionários para as outras listas.
        /// </remarks>
        /// <returns>Devolve a lista de Paises</returns>
        public List<Country> GetData()
        {
            List<Country> countries = new List<Country>();
            Dictionary<string, List<string>> timezones = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> topleveldomains = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> callingcodes = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> borders = new Dictionary<string, List<string>>();


            Dictionary<string, List<Language>> languages = new Dictionary<string, List<Language>>();
            Dictionary<string, Translations> translations = new Dictionary<string, Translations>();
            Dictionary<string, List<Currency>> currencies = new Dictionary<string, List<Currency>>();
            Dictionary<string, List<double>> latlng = new Dictionary<string, List<double>>();



            try
            {

                //----------------------------------------------------------------------------------------=
                // TabelaToDict...() -> carrega dicionarios com os registos das tabelas ( key --> value ) | 
                // definição mais abaixo                                                                  | 
                //----------------------------------------------------------------------------------------=

                timezones = TabelaToDictStr("select * from Timezones", "Timezone");
                topleveldomains = TabelaToDictStr("select * from TopLevelDomain", "Domain");
                callingcodes = TabelaToDictStr("select * from CallingCodes", "Code");
                borders = TabelaToDictStr("select * from Borders", "Border");
                
                currencies = TabelaToDictCurr("select * from Currencies", "Name", "Code", "Symbol");
                languages = TabelaToDictLang("select * from Languages", "Name");
                translations = TabelaToDictTrans("select * from Translations", "pt");
                latlng = TabelaToDictLatlng("select * from Latlng", "Lat", "Lng");

                //foreach (KeyValuePair<string, List<string>> brdr in borders)
                //{
                //    MessageBox.Show(string.Format("Key = {0}, Value = {1}",
                //        brdr.Key, string.Join(", ", brdr.Value)));
                //}



                string sql =
                "select * from Countries";
                //string sql =
                //    "select Countries.*, TopLevelDomain.Domain, Timezones.Timezone from Countries " +
                //    "inner join TopLevelDomain on TopLevelDomain.Alpha2Code = Countries.Alpha2Code " +
                //    "inner join Timezones on Timezones.Alpha2Code = Countries.Alpha2Code";

                command = new SQLiteCommand(sql, connection);
                
                //Lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();


                //Carrega da BD para a lista
                
                while (reader.Read()) //retorna um bool quando não existe registo é falso
                {
                    
                    countries.Add(new Country
                    {
                        Name = (string) reader["Name"],
                        Alpha2Code = (string) reader["Alpha2Code"],
                        Alpha3Code = (string) reader["Alpha3Code"],
                        Capital = (string) reader["Capital"],
                        Region = (string) reader["Region"],
                        Subregion = (string) reader["Subregion"],
                        Population = (int) reader["Population"],
                        Area = reader["Area"] is DBNull ? 0 : (double) reader["Area"],
                        NativeName = (string)reader["NativeName"],
                        //FlagBytes = (byte[])reader["FlagBytes"],
                        Timezones = timezones[(string) reader["Alpha2Code"]],
                        TopLevelDomain = topleveldomains[(string) reader["Alpha2Code"]],
                        CallingCodes = callingcodes[(string) reader["Alpha2Code"]],
                        Borders = borders[(string) reader["Alpha2Code"]],
                        Languages = languages[(string) reader["Alpha2Code"]],
                        Translations = translations[(string) reader["Alpha2Code"]],
                        Currencies = currencies[(string) reader["Alpha2Code"]],
                        Latlng = latlng[(string) reader["Alpha2Code"]]
                    });
                    
                }
                connection.Close();

                //MessageBox.Show(currencies["AX"].ToString());
                return countries;

            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);

                return null;
            }
        }


        //Recebe os valores da tabelas e carrega para um Dict<> de valores 'string'
        private Dictionary<string, List<string>> TabelaToDictStr(string sql, string field)
        {
             command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            var keyAct = "";
            var valueAct = "";
            List<string> listAux;
            var setKeys = new HashSet<string>();
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>> ();

            while (reader.Read())
            {
                if (reader[field] is DBNull)
                    valueAct = "";
                else
                    valueAct = (string)reader[field];

                keyAct = (string)reader["Alpha2Code"];

                if (setKeys.Add(keyAct))
                {
                    listAux = new List<string>();
                    dict.Add(keyAct, listAux);
                }
                dict[keyAct].Add(valueAct);
            }
            return dict;
        }


        //Recebe os valores da tabela 'Currencies' e carrega para um Dict<> de valores 'Currency'
        private Dictionary<string, List<Currency>> TabelaToDictCurr(string sql, params string[] fields)
        {

            command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            var keyAct = "";
            Currency valueAct = new Currency(); 
            List<Currency> listAux;
            var setKeys = new HashSet<string>();
            Dictionary<string, List<Currency>> dict = new Dictionary<string, List<Currency>>();

            while (reader.Read())
            {
                valueAct = new Currency
                {
                    Name = reader[fields[0]] is DBNull ? "" : (string)reader[fields[0]],
                    code = reader[fields[1]] is DBNull ? "" : (string)reader[fields[1]],
                    Symbol = reader[fields[2]] is DBNull ? "" : (string)reader[fields[2]]
                } ;

                keyAct = (string)reader["Alpha2Code"];

                if (setKeys.Add(keyAct))
                {
                    listAux = new List<Currency>();
                    dict.Add(keyAct, listAux);
                }
                dict[keyAct].Add(valueAct);

            }

            return dict;
        }


        //Recebe os valores da tabela 'Languages' e carrega para um Dict<> de valores 'Language'
        private Dictionary<string, List<Language>> TabelaToDictLang(string sql, params string[] fields)
        {

            command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            var keyAct = "";
            Language valueAct = new Language();
            List<Language> listAux;
            var setKeys = new HashSet<string>();
            Dictionary<string, List<Language>> dict = new Dictionary<string, List<Language>>();

            while (reader.Read())
            {
                foreach(var f in fields)
                {
                    valueAct = new Language
                    {
                        Name = (string)reader[f]
                    };
                }

                keyAct = (string)reader["Alpha2Code"];

                if (setKeys.Add(keyAct))
                {
                    listAux = new List<Language>();
                    dict.Add(keyAct, listAux);
                }
                dict[keyAct].Add(valueAct);

            }

            return dict;
        }


        //Recebe os valores da tabela 'Translations' e carrega para um Dict<> de valores 'Translations'
        private Dictionary<string, Translations> TabelaToDictTrans(string sql, string field)
        {

            command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            var keyAct = "";
            var valueAct = new Translations();
            Dictionary<string, Translations> dict = new Dictionary<string, Translations>();

            while (reader.Read())
            {
                valueAct = new Translations
                {
                    pt = (string)reader[field],
                };
                keyAct = (string)reader["Alpha2Code"];

                dict.Add(keyAct, valueAct);
            }

            return dict;
        }


        //Recebe os valores da tabela 'Latlng' e carrega para um Dict<> de pares de valores 'double'
        private Dictionary<string, List<double>> TabelaToDictLatlng(string sql, string field1, string field2)
        {

            command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            var keyAct = "";
            List<double> valueAct;
            var setKeys = new HashSet<string>();
            Dictionary<string, List<double>> dict = new Dictionary<string, List<double>>();

            while (reader.Read())
            {
                valueAct = new List<double>();

                valueAct.Add((double)reader[field1]);
                valueAct.Add((double)reader[field2]);

                keyAct = (string)reader["Alpha2Code"];


                dict.Add(keyAct, valueAct);

            }

            return dict;
        }


        /// <summary>
        /// Método exclusivo para o receção dos bytes das imagens das bandeiras.
        /// </summary>
        /// <param name="alfa2">Utiliza o identificador único alfa2code (chave) para ir buscar a bandeira do país certo.</param>
        /// <returns>Devolve as bandeiras em bytes.</returns>
        public byte[] BlobReader(string alfa2)
        {
            connection = new SQLiteConnection("Data Source=Data/Paises.sqlite");
            connection.Open(); 

            string sql = "select * from Countries";

            command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            byte[] blob = new byte[0];

            while (reader.Read())
                if ((string)reader["Alpha2Code"] == alfa2)
                {
                    blob = (byte[])reader["FlagBytes"];
                    break;
                }
            return blob;
        }


        /// <summary>
        /// Método que limpa os dados das tabelas antes de atualizar.
        /// </summary>
        public void DeleteData()
        {
            try
            {
                string sql = "delete from Borders;" +
                    "delete from CallingCodes;" +
                    "delete from Countries;" +
                    "delete from Currencies;" +
                    "delete from Languages;" +
                    "delete from Latlng;" +
                    "delete from Timezones;" +
                    "delete from TopLevelDomain;" +
                    "delete from Translations;";

                command = new SQLiteCommand(sql, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }
    }

}
