﻿
namespace Paises.Servicos
{
    using Svg;
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Windows.Forms;

    public class ImageService
    {
        /// <summary>
        /// Converte/lê o ficheiro svg para um array de bytes.
        /// </summary>
        /// <param name="link"></param>
        /// <returns>Devolve um array de bytes</returns>
        public byte[] SvgToByteArray(string link)
        {
            //salva o ficheiro svg localmente
            WebClient webClient = new WebClient();
            webClient.DownloadFile(link, @"images\localFileName");
            //byte[] imageBytes = webClient.DownloadData(paisB.Flag); //prob com svg

            //lê o svg local
            var svgDocument = SvgDocument.Open(@"images\localFileName");

            Bitmap flag = new Bitmap(200, 150);

            try
            {
                flag = svgDocument.Draw();
                
                //salva o fich convertido do svg para o tipo definido
                //flag.Save(@"images\teste.png", ImageFormat.Png);
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }

            ImageConverter converter = new ImageConverter();

            return (byte[])converter.ConvertTo(flag, typeof(byte[]));
        }


        /// <summary>
        /// Lê a informação em bytes e devolve o ficheiro Imagem.
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns>Objecto Image</returns>
        public Image byteArrayToSvg(byte[] byteArray)
        {
            //provisório, filler
            MemoryStream ms = new MemoryStream(byteArray);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }


        /// <summary>
        /// Converte um ficheiro svg para o formato png.
        /// </summary>
        /// <remarks>
        /// Não implementado no projecto.
        /// </remarks>
        /// <param name="link"></param>
        public void SvgToPng(string link)
        {

            WebClient webClient = new WebClient();
            webClient.DownloadFile(link, @"images\localFileName");

            var svgDocument = SvgDocument.Open(@"images\localFileName");

            Bitmap flag = new Bitmap(200, 150);

            flag.Save(@"images\teste.png", ImageFormat.Png);
 
        }


        /// <summary>
        /// Possibilita o redimensionamento de uma imagem.
        /// </summary>
        /// <remarks>
        /// Semi-implementado, permitia o 'resize' de todo o UI através de um ratio. Mas posto de lado, para já.
        /// </remarks>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns>Devolve objecto Bitmap.</returns>
        public Bitmap ResizeImage(Image image, int width, int height)
        {
            var imgRect = new Rectangle(0, 0, width, height);
            var resultImage = new Bitmap(width, height);

            resultImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(resultImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, imgRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return resultImage;
        }



    }
}
